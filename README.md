### Sila Foods

This project was Developed using React native and currently ongoing

#### Project Description
Project is based upon the food ordering application and is basically used for online ordering of food and grocery items, The images attached are not the final product however the details attached are only for display purpose

#### Screenshots
Below are some of the Project Screenshots for frontend of the project
Developed using React Native

![alt text](http://rodionsolutions.com/images/banner.png)

![alt text](http://rodionsolutions.com/assets/images/silafoods/silafoods_6.jpeg "Order page")

![alt text](http://rodionsolutions.com/assets/images/silafoods/silafoods_5.jpeg "Order page")

![alt text](http://rodionsolutions.com/assets/images/silafoods/silafoods_4.jpeg "Order page")

![alt text](http://rodionsolutions.com/assets/images/silafoods/silafoods_3.jpeg "Order page")

![alt text](http://rodionsolutions.com/assets/images/silafoods/silafoods_2.jpeg "Order page")

![alt text](http://rodionsolutions.com/assets/images/silafoods/silafoods_1.jpeg "Order page")

#### Running the app

##### For android type the following command "react-native run-android"
##### For ios type the following command "react-native run-ios"

Note: You need to have all the gradle and android or ios tooling installed inorder to be able to run the project.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or contact us 
[Rodion Solutions](http:rodionsolutions.com).
[Email]: info@rodionsolutions.com
